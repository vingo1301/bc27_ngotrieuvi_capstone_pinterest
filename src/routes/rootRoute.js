const express = require('express');
const { imgRoute } = require('./imgRoute');
const userRoute = require('./userRoute');
const rootRoute = express.Router()

rootRoute.use("/user",userRoute)
rootRoute.use("/img",imgRoute)

module.exports = rootRoute;