const express = require('express');
const { createUser, getUser, login, getUserById, updateUser, changePassword } = require('../controller/userController');
const { auth } = require('./middleWare');
const userRoute = express.Router();

//  Đăng ký
userRoute.post("/signUp",createUser)
// Lấy danh sách user
userRoute.get("/getUser",auth,getUser)
// Lấy thông tin user theo id
userRoute.get("/getUserById/:id",auth,getUserById)
// Đăng Nhập
userRoute.post("/signIn",login)
// Đổi thông tin cá nhân
userRoute.put("/updateUser",auth,updateUser)
// Đổi mật khẩu
userRoute.put("/changePassword",auth,changePassword)

module.exports = userRoute;