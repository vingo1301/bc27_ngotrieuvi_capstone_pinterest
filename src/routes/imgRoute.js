const express = require("express");
const { createPicture, getPicture, getPictureByName, getPictureById, createComment, getComment, checkSaved, savePicture, getPictureByUser, deletePicture, getSavedByUser } = require("../controller/imgController");
const { auth, upload } = require("./middleWare");
const imgRoute = express.Router()

// Thêm ảnh
imgRoute.post("/post",auth,upload.single("image"),createPicture);
//  Lấy danh sách ảnh
imgRoute.get("/get",auth,getPicture)
// Tìm ảnh theo tên
imgRoute.get("/getImgByName/:key",auth,getPictureByName)
//  Lấy ảnh theo Id
imgRoute.get("/getImgById/:id",auth,getPictureById)
//  Lấy ảnh đã tạo theo user
imgRoute.get("/getImgByUser/:id",auth,getPictureByUser)
// Lấy ảnh đã lưu theo user
imgRoute.get("/getSavedByUser/:id",auth,getSavedByUser)
// Đăng bình luận
imgRoute.post("/DangBinhLuan",auth,createComment)
// Lấy Bình Luận
imgRoute.get("/getComment/:id",auth,getComment)
//  Kiểm tra ảnh lưu chưa
imgRoute.get("/checkSaved/:id",auth,checkSaved)
// Lưu ảnh
imgRoute.post("/savePicture/:id",auth,savePicture)
//  Xoá ảnh
imgRoute.delete("/deletePicture/:id",auth,deletePicture)

 module.exports = {imgRoute}