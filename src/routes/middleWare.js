const jwt = require('jsonwebtoken');
const multer= require('multer')
const { checkToken } = require('../jwt');

let auth = (req,res,next) => {
    try{
        const {token} = req.headers;
        const data = checkToken(token);
        if(data){
            req.user = data;
            next()
        }
    }catch(err){
        res.status(401).send(err)
    }
    
}
const storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null,"./public/img");
    },
    filename: (req,file,cb) => {
        const name = Date.now() + "_" + file.originalname;
        cb(null,name)
    }
})
const upload = multer({storage})
module.exports = {auth,upload}