const jwt = require('jsonwebtoken');
let createToken  = (data) => {
    return jwt.sign({data},"pinterest",{expiresIn:"30d"})
}
let checkToken = (token) => {
    return jwt.verify(token,"pinterest")
}
module.exports = {createToken,checkToken}