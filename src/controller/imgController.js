const {PrismaClient} = require('@prisma/client');
const fs = require('fs');
const prisma = new PrismaClient()

const createPicture = async (req,res) => {
    try{
        let {filename,destination} = req.file;
        let user = req.user.data;
        let {ten_hinh,mo_ta} = req.body;
        let data = {
            ten_hinh,
            duong_dan:destination + "/" + filename,
            mo_ta,
            nguoi_dung_id: Number(user.nguoi_dung_id)
        }
        const createPicture = await prisma.hinh_anh.create({data});
        res.status(200).json({
            message: "Thêm ảnh thành công!",
            data: data
    })
    }catch(err){
        res.status(500).send(err);
    }
}
const getPicture = async (req,res) => {
    let data = await prisma.hinh_anh.findMany()
    res.status(200).send(data)
}
const getPictureByName = async (req,res) => {
    try{
        let searchKey = req.params.key
    let data = await prisma.hinh_anh.findMany({
        where:{
            ten_hinh:{
                contains: searchKey
            }
        }
    })
    if(data.length == 0){
        res.status(400).send("Không tìm thấy hình!")
    }else{
        res.status(200).send(data)
    }
    }
    catch(err){
        res.status(500).send(err)
    }
    
}
const getPictureById = async (req,res) => {
    try{
        let {id} = req.params;
        id = Number(id)
        let data = await prisma.hinh_anh.findFirst({
            where:{
                hinh_id: id
            },
            include:{
                nguoi_dung: {
                    select : {
                        email:true,
                        ho_ten:true,
                        anh_dai_dien:true
                    }
                }
            }
        })
        if(data == null){
            res.status(400).send("Không tìm thấy hình!")
        }else  res.status(200).send(data)
    }catch(err){
        res.status(500).send(err)
    }
}
const getPictureByUser = async (req,res) => {
    try{
        let {id} = req.params;
        id = Number(id);
        let data = await prisma.hinh_anh.findMany({
            where:{
                nguoi_dung_id: id
            }
        })
        if(data.length == 0){
            res.status(400).send("Không tìm thấy người dùng hoặc người dùng này chưa đăng ảnh!")
        }else{
            res.status(200).send(data);
        }
    }catch(err){
        res.status(500).send(err)
    }
}
const getSavedByUser = async (req,res) => {
    try{
        let {id} = req.params;
        id = Number(id);
        let data = await prisma.luu_anh.findMany({
            where:{
                nguoi_dung_id: id
            }
        })
        if(data.length == 0){
            res.status(400).send("Không tìm thấy người dùng hoặc người dùng này chưa lưu ảnh!")
        }else{
            res.status(200).send(data);
        }
    }catch(err){
        res.status(500).send(err)
    }
}
const createComment = async (req,res) => {
    try{
        user = req.user.data;
    let {hinh_id,noi_dung} = req.body;
    let binhLuan = {
        nguoi_dung_id : user.nguoi_dung_id,
        hinh_id,
        noi_dung,
        ngay_binh_luan: new Date()
    }
    let index = await prisma.hinh_anh.findFirst({
        where:{
            hinh_id
        }
    })
    if(index == null){
        res.status(400).send("Không tìm thấy hình ảnh!");
    }else{
        await prisma.binh_luan.create({data:binhLuan});
        res.status(200).json({
            message : "Đăng bình luận thành công!",
            data : binhLuan
        });
    }
    }catch(err){
        res.status(500).send(err);
    }
}
const getComment = async (req,res) => {
    try{
        let {id} = req.params;
        let comment = await prisma.binh_luan.findMany({
        where:{
            hinh_id:Number(id)
        },
        include: {
            nguoi_dung:{
                select:{
                    ho_ten:true,
                    anh_dai_dien:true
                }
            }
        }
    })
    console.log("comment: ",comment);
    if(comment.length == 0){
        res.status(401).send("Không tìm thấy ảnh hoặc ảnh này chưa có bình luận!");
    }else{
        res.status(200).send(comment);
    }
    }catch(err){
        res.status(500).send(err);
    }
}
const checkSaved = async (req,res) => {
    try{
        let {id} = req.params;
    let check = await prisma.luu_anh.findFirst({
        where:{
            hinh_id: Number(id)
        }
    })
    if(check == null){
        res.status(401).send("Không tìm thấy ảnh hoặc ảnh chưa được lưu!")
    }else{
        res.status(200).json({
            message: "Ảnh đã được lưu!",
            data: check
        })
    }
    }catch(err){
        res.status(500).send(err);
    }
}
const savePicture = async (req,res) => {
    try{
        let {id} = req.params;
        let data = await prisma.hinh_anh.findFirst({
            where:{
                hinh_id: Number(id)
            }
        })
        if(data == null){
            res.status(401).send("Không tìm thấy hình!")
        }else{
            let index = await prisma.luu_anh.findFirst({
                where:{
                    hinh_id: Number(id)
                }
            })
            if(index == null){
                let picture = {
                    nguoi_dung_id: data.nguoi_dung_id,
                    hinh_id: data.hinh_id,
                    ngay_luu: new Date()
                }
                await prisma.luu_anh.create({data:picture});
                res.status(200).json({
                    message: "Lưu ảnh thành công!",
                    data: picture
                })
            }else res.status(401).send("Ảnh đã được lưu rồi!")
        }
    }catch(err){
        res.status(500).send(err);
    }
}
const deletePicture = async (req,res) => {
    try{
        let {id} = req.params;
        let index = await prisma.hinh_anh.findFirst({
            where:{
                hinh_id: Number(id)
            }
        })
        if(index == null){
            res.status(400).send("Không tìm thấy ảnh!")
        }else{
            
            await prisma.hinh_anh.delete({
                where:{
                    hinh_id: Number(id)
                }
            });
            // Xoá ảnh trên máy. 
            // fs.unlink(index.duong_dan,function(err){
            //     if (err) throw err;
            //     console.log('File deleted!');
            // })
            res.status(200).send("Xoá ảnh thành công!")
        }
    }catch(err){
        res.status(500).send(err)
    }
}
module.exports = {createPicture,getPicture,getPictureByName,getPictureById,createComment,getComment,checkSaved,savePicture,getPictureByUser,getSavedByUser,deletePicture}