const {PrismaClient} = require('@prisma/client')
const prisma = new PrismaClient()
const bcrypt = require("bcrypt");
const { createToken } = require('../jwt');

const getUser = async (req,res) => {
    let user = await prisma.nguoi_dung.findMany();
    user.map((item) => {
        item.mat_khau = "";
    })
    res.status(200).send(user)
}
const getUserById = async (req,res) => {
    try{
        let {id} = req.params;
    let user = await prisma.nguoi_dung.findFirst({
        where:{
            nguoi_dung_id: Number(id)
        }
    })
    if (user == null){
        res.status(401).send("Không tìm thấy user!");
    }else{
        user.mat_khau = "";
        res.status(200).send(user);
    }
    }catch(err){
        res.status(500).send(err);
    }
}
const createUser = async (req,res) => {
    let data = req.body;
    data.mat_khau = await bcrypt.hash(data.mat_khau, 10);
    let checkEmail = await prisma.nguoi_dung.findFirst({
        where : {
            email:data.email
        }
    })
    if (checkEmail){
        res.status(401).send("Email Đã Tồn Tại!");
    }
    else{
        await prisma.nguoi_dung.create({data})
        data.mat_khau = ""
        res.status(200).json({
        message:"Đăng ký thành công!",
        data: data
        })
    }
}
const login = async (req,res) => {
    let {email,password} = req.body;
    let checkEmail = await prisma.nguoi_dung.findFirst({
        where:{
            email
        }
    })
    if(checkEmail == null){
        res.status(401).send("Không tìm thấy email!")
    }else{
        checkPassword = await bcrypt.compare(password,checkEmail.mat_khau);
        if(checkPassword){
            checkEmail.mat_khau = "";
            let token = createToken(checkEmail);
            checkEmail = {...checkEmail,token}
            res.status(200).json({
                message: "Đăng nhập thành công!",
                content: checkEmail
            })
        }
        else res.status(401).send("Mật khẩu không chính xác!")
    }
    
}
const updateUser = async (req,res) => {
    try{
        let user = req.user.data;
        let {ho_ten,anh_dai_dien,tuoi} = req.body;
        let updateInfo = {ho_ten,anh_dai_dien,tuoi}
        let update = await prisma.nguoi_dung.update({data:updateInfo,
        where:{
            nguoi_dung_id: user.nguoi_dung_id
        }})
        res.status(200).json({
            message: "update thành công!",
            data: update
        })
    }catch(err){
        res.status(500).send(err)
    }
}
const changePassword = async (req,res) => {
    try{
        let user = req.user.data;
        let {mat_khau} = req.body;
        if(mat_khau == null || mat_khau == ""){
            res.status(400).send("Không được để trống!")
        }
        else{
            mat_khau = await bcrypt.hash(mat_khau,10);
            await prisma.nguoi_dung.update({data:{mat_khau},
            where:{
                nguoi_dung_id: user.nguoi_dung_id
            }})
        res.status(200).send("Đổi mật khẩu thành công!");
        }
    }catch(err){
        res.status(500).send(err)
    }
}
module.exports = {createUser, getUser,login,getUserById,updateUser,changePassword}